// $Id$
// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import java.io.File;

import net.sf.saxon.s9api.Serializer;

import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterStep;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.cores9api.PipelinedComparatorS9;
import com.deltaxml.pipe.filters.NormalizeSpace;

public class DemoPipeline {

  PipelinedComparatorS9 pc;

  boolean runADefault= true;
  boolean dontRunBDefault= false;
  boolean tidyInputsDefault= true;
  boolean inputAlreadyTidyDefault= false;

  String normalizeAttValuesDefault= "true";

  public void setUpPipeline() throws Exception {
    setUpPipeline(runADefault, dontRunBDefault, tidyInputsDefault, inputAlreadyTidyDefault, normalizeAttValuesDefault);
  }

  public void setUpPipeline(boolean runA, boolean dontRunB, boolean tidyInputs, boolean inputAlreadyTidy,
                            String normalizeAttValues) throws Exception {
    pc= new PipelinedComparatorS9();
    FilterStepHelper fsh= pc.newFilterStepHelper();

    // Create and configure input chain 1.
    FilterChain inChain1= fsh.newFilterChain();
    FilterStep stepTidyInput1= fsh.newFilterStep(new File("tidy-input.xsl"), "tidy-input");
    inChain1.addStep(stepTidyInput1);

    FilterStep stepInput1FilterA= fsh.newFilterStep(new File("input-filterA.xsl"), "in-filterA");
    inChain1.addStep(stepInput1FilterA);

    FilterStep stepInput1FilterB= fsh.newFilterStep(new File("input-filterB.xsl"), "in-filterB");
    inChain1.addStep(stepInput1FilterB);

    FilterStep stepInput1ParamFilter= fsh.newFilterStep(new File("input-filter-with-parameter.xsl"), "in-param-filter");
    stepInput1ParamFilter.setParameterValue("an-input-param", "input1");
    inChain1.addStep(stepInput1ParamFilter);

    // Create and configure input chain 2.
    FilterChain inChain2= fsh.newFilterChain();
    FilterStep stepTidyInput2= fsh.newFilterStep(new File("tidy-input.xsl"), "tidy-input");
    inChain2.addStep(stepTidyInput2);

    FilterStep stepInput2FilterA= fsh.newFilterStep(new File("input-filterA.xsl"), "in-filterA");
    inChain2.addStep(stepInput2FilterA);

    FilterStep stepInput2ParamFilter= fsh.newFilterStep(new File("input-filter-with-parameter.xsl"), "in-param-filter");
    inChain2.addStep(stepInput2ParamFilter);
    // It is also possible to get the step from a chain and set its value.
    // Note that the value can be changed at any time before a comparison, but should
    // not be updated during a comparison.
    FilterStep step= inChain2.getStep("in-param-filter");
    step.setParameterValue("an-input-param", "input2");

    // Create and configure the output filter chain.
    FilterChain outChain= fsh.newFilterChain();
    FilterStep stepNormalize= fsh.newFilterStep(NormalizeSpace.class, "normalize-att-filter");
    stepNormalize.setParameterValue("normalizeAttValues", normalizeAttValues);
    outChain.addStep(stepNormalize);

    // Enable or disable filters as required
    // Note that filter steps are created enabled so the lines setting to true are not necessary
    stepTidyInput1.setEnabled(tidyInputs && !inputAlreadyTidy);
    stepInput1FilterA.setEnabled(runA);
    stepInput1FilterB.setEnabled(!dontRunB);
    stepInput1ParamFilter.setEnabled(true);

    stepTidyInput2.setEnabled(tidyInputs && !inputAlreadyTidy);
    stepInput2FilterA.setEnabled(true);
    stepInput2ParamFilter.setEnabled(true);

    stepNormalize.setEnabled(true);

    // Add the chains to the pipelined comparator
    pc.setInput1Filters(inChain1);
    pc.setInput2Filters(inChain2);
    pc.setOutputFilters(outChain);

    // Setup the output, parser, and comparator features and properties.
    pc.setOutputProperty(Serializer.Property.INDENT, "yes");
    pc.setParserFeature("http://apache.org/xml/features/xinclude", true);
    pc.setComparatorFeature("http://deltaxml.com/api/feature/isFullDelta", true);
    pc.setComparatorProperty("http://deltaxml.com/api/property/orderlessPresentation", "a_matches_deletes_adds");
  }

  public void runComparison(File input1, File input2, File result) throws Exception {
    pc.compare(input1, input2, result);
  }

  public static void main(String[] args) throws Exception {
    System.out.println("Running pipeline with default parameter settings...");
    DemoPipeline demo= new DemoPipeline();
    demo.setUpPipeline();
    demo.runComparison(new File("demo-input.xml"), new File("demo-input.xml"), new File("result.xml"));
  }

}
